##Median
The median is the value separating the higher half of a data sample, a population, or a probability distribution, from the lower half[1]. Code[2].

##References
1. Median, wikipedia, https://en.wikipedia.org/wiki/Median

2. Quick Sort, Drozdek, http://www.mathcs.duq.edu/drozdek/DSinCpp/sorts.h