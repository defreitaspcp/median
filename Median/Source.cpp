/*
Median Algorithm
References
[1] Quick Sort, Drozdek, http://www.mathcs.duq.edu/drozdek/DSinCpp/sorts.h
[2] Median, wikipedia, https://en.wikipedia.org/wiki/Median
*/
#include<iostream>
using namespace std;
struct Sort
{
public:
	void quicksort(double data[], int n)
	{
		int i, max;
		if (n < 2)
		{
			return;
		}
		for (i = 1, max = 0; i < n; i++)// find the largest
		{
			if (data[max] < data[i])// element and put it
			{
				max = i;                // at the end of data[];
			}
		}
		swap(data, n - 1, max); // largest el is now in its
		quicksort(data, 0, n - 2);     // final position;
	}
private:
	void swap(double data[], int i, int j)
	{
		double temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}
	void quicksort(double data[], int first, int last)
	{
		int lower = first + 1, upper = last;
		swap(data, first, (first + last) / 2);
		double bound = data[first];
		while (lower <= upper)
		{
			while (data[lower] < bound)
			{
				lower++;
			}
			while (bound < data[upper])
			{
				upper--;
			}
			if (lower < upper)
			{
				swap(data, lower++, upper--);
			}
			else
			{
				lower++;
			}
		}
		swap(data, upper, first);
		if (first < upper - 1)
		{
			quicksort(data, first, upper - 1);
		}
		if (upper + 1 < last)
		{
			quicksort(data, upper + 1, last);
		}
	}
};
double Median(double data[], int length)
{
	Sort sort;
	sort.quicksort(data, length);
	if (length % 2 == 0)
	{
		return (data[length / 2 - 1] + data[length / 2]) / 2;
	}
	else
	{
		return data[length / 2];
	}
}
int main()
{
	double a[5] = { 32, 27, 15, 44, 15 };
	double b[6] = { 32, 27, 15, 44, 15, 32 };
	cout << Median(a, 5) << endl;
	cout << Median(b, 6) << endl;
	return 0;
}